package bikproxy.bik;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class BikPut {

    private static final int BUFFER_SIZE = 4096;

    private String ftp = null;
    private String host = null;
    private String user = null;
    private String pass = null;
    private String path = null;

    public BikPut(String ftp, String host, String user, String pass, String path) {
        this.ftp = ftp;
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.path = path;
    }

    public void put(String file) {

        ftp = String.format(ftp, user, pass, host, path);

        try {
            URL url = new URL(ftp);
            URLConnection connection = url.openConnection();
            OutputStream output = connection.getOutputStream();
            FileInputStream input = new FileInputStream(file);

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            while ((bytesRead = input.read(buffer)) != -1) {
                output.write(buffer,0, bytesRead);
            }

            input.close();
            output.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
