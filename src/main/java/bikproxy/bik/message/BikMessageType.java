package bikproxy.bik.message;

public enum BikMessageType {
    creditApplicationReportRequest,
    creditApplicationReportResponse,
    creditApplicationReportCredentials,

    entrepreneurCreditReportRequest,
    entrepreneurCreditReportResponse,
    entrepreneurCreditReportCredentials,

    clientMonitoringReportRequest,
    clientMonitoringReportResponse,
    clientMonitoringReportCredentials,

    entrepreneurMonitoringReportRequest,
    entrepreneurMonitoringReportResponse,
    entrepreneurMonitoringReportCredentials,

    clientManagementReportRequest,
    clientManagementReportResponse,
    clientManagementReportCredentials,

    updateRequest,
    updateResponse,
    updateCredentials,

    processingError,
    validationError,
    updateValidationError,
    authorisationError
}
