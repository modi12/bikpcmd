package bikproxy.bik.message;

import javax.xml.soap.*;

public class BikMessage {

    public BikMessage() {
    }


//    creditApplicationReportRequest
//            creditApplicationReportResponse
//    creditApplicationReportCredentials
//            entrepreneurCreditReportRequest
//    entrepreneurCreditReportResponse
//            entrepreneurCreditReportCredentials
//    clientMonitoringReportRequest
//            clientMonitoringReportResponse
//    clientMonitoringReportCredentials
//            entrepreneurMonitoringReportRequest
//    entrepreneurMonitoringReportResponse
//            entrepreneurMonitoringReportCredentials
//    clientManagementReportRequest
//            clientManagementReportResponse
//    clientManagementReportCredentials
//            updateRequest
//    updateResponse
//            updateCredentials
//    processingError
//            validationError
//    updateValidationError
//            authorisationError

    private static SOAPMessage createBikMessage(BikMessageType type) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://dot.com/"; // TODO

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("namespace", serverURI); // TODO

        /*
        Constructed SOAP Request Message:
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:example="http://ws.cdyne.com/">
            <SOAP-ENV:Header/>
            <SOAP-ENV:Body>
                <example:VerifyEmail>
                    <example:email>mutantninja@gmail.com</example:email>
                    <example:LicenseKey>123</example:LicenseKey>
                </example:VerifyEmail>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>
         */

        // SOAP Body - TODO
        SOAPBody soapBody = envelope.getBody();
        SOAPElement soapBodyElem = soapBody.addChildElement("VerifyEmail", "example");
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("email", "example");
        soapBodyElem1.addTextNode("mail@gmail.com");
        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("LicenseKey", "example");
        soapBodyElem2.addTextNode("text");

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI  + "VerifyEmail");

        soapMessage.saveChanges();

        System.out.print("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }
}

