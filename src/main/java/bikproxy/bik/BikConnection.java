package bikproxy.bik;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.io.IOException;

public class BikConnection {

    private static BikConnection bikConnection;

    private SOAPConnectionFactory soapConnectionFactory;
    private SOAPConnection soapConnection;

    private BikConnection() {
        try {
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }

    public static BikConnection instance(){
        if(bikConnection == null){
            bikConnection = new BikConnection();
        }
        return bikConnection;
    }

    public void sendSoap(SOAPMessage message, String url) {
        SOAPMessage soapResponse = null;
        try {
            soapResponse = soapConnection.call(message, url);
        } catch (SOAPException e) {
            e.printStackTrace();
        }

        try {
            soapResponse.writeTo(System.out);
        } catch (SOAPException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void connect(){
        try {
            soapConnection = soapConnectionFactory.createConnection();
        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() throws SOAPException {
        soapConnection.close();
    }
}
