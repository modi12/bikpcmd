package bikproxy.bik.input;

public class BikCreateInput {

    BikCreateInput() {
    }

    BikInput getInput(BikAccount create, BikAccount update, BikAccount remove, BikAccount close) {

        BikInput input;

        if (update == null && close == null) {
            input = new BikInputNz();
        }
        else {
            input = new BikInputFull();
        }

        // TODO

        return input;
    }
}
