package bikproxy.bik;

import bikproxy.bik.BikData;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BikRepository extends JpaRepository<BikData, Long> {
}
