package bikproxy.rest;

public class RestItem {

    private final long id;
    private final Integer content;

    public RestItem(long id, Integer content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public Integer getContent() {
        return content;
    }
}
