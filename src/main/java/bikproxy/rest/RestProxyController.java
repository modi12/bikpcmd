package bikproxy.rest;

import bikproxy.bik.BikData;
import bikproxy.bik.BikRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class RestProxyController {

    @Autowired
    private BikRepository bikRepository;
    private final AtomicLong counter = new AtomicLong();
    List<BikData> list;

    @RequestMapping("/")
    public String home() {
        return " __ home __";
    }

    @RequestMapping("/name")
    public RestName name(@RequestParam(value = "value", defaultValue = "no-name") String val) {
        return new RestName(counter.incrementAndGet(), val);
    }

    @RequestMapping("/item")
    public RestItem item(@RequestParam(value = "value", defaultValue = "0") Integer val) {
        return new RestItem(counter.incrementAndGet(), val);
    }

    @RequestMapping(value = "item", method = RequestMethod.GET)
    public Integer getItem() {
        return counter.intValue();
    }


    /**
     * DATA
     *
     * */
    @RequestMapping(value = "data", method = RequestMethod.GET)
    public List<BikData> list () {
        return list;
    }

    @RequestMapping(value = "data", method = RequestMethod.POST)
    public BikData create (@RequestBody BikData data){
        return bikRepository.saveAndFlush(data);
    }

    @RequestMapping(value = "data/{id}", method = RequestMethod.GET)
    public BikData get (@PathVariable Long id){
        return bikRepository.findOne(id);
    }

    @RequestMapping(value = "data/{id}", method = RequestMethod.PUT)
    public BikData update (@PathVariable Long id, @RequestBody BikData data){
        BikData oneData = bikRepository.findOne(id);
        BeanUtils.copyProperties(data, oneData);
        return bikRepository.saveAndFlush(data);
    }

    @RequestMapping(value = "data/{id}", method = RequestMethod.DELETE)
    public BikData delete (@PathVariable Long id){
        BikData data = bikRepository.findOne(id);
        bikRepository.delete(data);
        return data;
    }




//    @RequestMapping("/error")
//    public RestErr err(@RequestParam(value = "value", defaultValue = "no-error") String val) {
//        return new RestErr(counter.incrementAndGet(), val);
//    }

    /**
     * This method will handle all the Number Format Exceptions that arise within this controller.
     *
     * */
    @ExceptionHandler(value = NumberFormatException.class)
    public String nfeHandler(NumberFormatException e){
        return e.getMessage();
    }
}
