package bikproxy.rest;

public class RestErr {

    private final long id;
    private final String err;

    public RestErr(long id, String err) {
        this.id = id;
        this.err = err;
    }

    public long getId() {
        return id;
    }

    public String getErr() {
        return err;
    }
}
